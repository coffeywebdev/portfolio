var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var port = 3000;
var app = express();

// Bodyparser Middleware
app.use(bodyParser.json());

// DB Config
var db = require('./config/keys').mongoURI;

// Connect to MongoDB
mongoose.connect(db, { useNewUrlParser: true })
    .then(function(){
        console.log('MongoDB connected...')
    })
    .catch(function(err){
        console.log(err);
    });


app.listen(port, function(){
    console.log(`server started on port ${port}`);
});

/* Alternatively, this is the "fat arrow" syntax

mongoose.connect(db, { useNewUrlParser: true })
    .then( () => console.log('MongoDB Connected...') )
    .catch( err => console.log(err));

 app.listen(port, () => console.log(`Server started on port ${port}`));

*/